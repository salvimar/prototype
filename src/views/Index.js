import {
    Container,
    Row,
    Col,
    Card,
    CardBody,
    CardTitle,
    CardImg,
    CardText, Button,
} from "reactstrap";
import Header from "../components/Headers/Header";
import axios from "axios";

let token;

function connect() {
    console.log("TEST Connect");
    window.location.href = 'http://localhost:3000/install';
}

export function getAccess() {
    return axios.get('http://localhost:3000/getAccess', {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
            }
        }
    )
        .then(response =>
            response
        );
}

const Index = (props) => {
    return (
        <>
            <Header/>
            <Container fluid>
                <Row>
                    <Col>
                        <Card className="shadow">
                            <div className={"cardContainer"}>
                                <img className={"cardlogo"}
                                     src={require("../assets/img/brand/hubspot-logo-64x64.png").default}/>
                                <CardTitle className={"cardTitle"}><h2>Hubspot</h2></CardTitle>
                            </div>

                            <CardBody>
                                <CardText>Hubspot is a unified platform for all your sales, marketing and customer
                                    service needs</CardText>
                                <Button onClick={getAccess}>Connect</Button>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card className="shadow">
                            <div className={"cardContainer"}>
                                <img className={"cardlogo"}
                                     src={require("../assets/img/brand/hubspot-logo-64x64.png").default}/>
                                <CardTitle className={"cardTitle"}><h2>Hubspot</h2></CardTitle>
                            </div>
                            <CardBody>
                                <CardText>Hubspot is a unified platform for all your sales, marketing and customer
                                    service needs</CardText>
                                <Button onClick={connect}>Connect</Button>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card className="shadow">
                            <div className={"cardContainer"}>
                                <img className={"cardlogo"}
                                     src={require("../assets/img/brand/hubspot-logo-64x64.png").default}/>
                                <CardTitle className={"cardTitle"}><h2>Hubspot</h2></CardTitle>
                            </div>
                            <CardBody>
                                <CardText>Hubspot is a unified platform for all your sales, marketing and customer
                                    service needs</CardText>
                                <Button>Connect</Button>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card className="shadow">
                            <div className={"cardContainer"}>
                                <img className={"cardlogo"}
                                     src={require("../assets/img/brand/hubspot-logo-64x64.png").default}/>
                                <CardTitle className={"cardTitle"}><h2>Hubspot</h2></CardTitle>
                            </div>
                            <CardBody>
                                <CardText>Hubspot is a unified platform for all your sales, marketing and customer
                                    service needs</CardText>
                                <Button>Connect</Button>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default Index;
