import {
    Card,
    CardHeader,
    CardFooter,
    Table,
    Container,
    Row,
    Button, Input,
} from "reactstrap";
import Header from "components/Headers/Header.js";
import axios from "axios";
import {useEffect, useState} from "react";


const Tables = () => {
    const [users, setUsers] = useState([]);
    const [userWelcomeEmail, setUserWelcomeEmail] = useState(false);
    const [userEmail, setUserEmail] = useState("");

    async function AddUser() {
        console.log("ADD USER");
        console.log(`userWelcomeEmail ==> ${userWelcomeEmail}`);
        console.log(`userEmail ==> ${userEmail}`);

        const result = await axios.post('http://localhost:3000/addUser', {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
                'Content-Type': 'application/json'
            },
            params: {
                'userEmail': userEmail,
                'userWelcomeEmail': userWelcomeEmail
            }
        }).then(async res => {
            setUserWelcomeEmail(false);
            setUserEmail('');

            if (res.data) {
                const result = await axios.get('http://localhost:3000/getAccess', {
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
                        }
                    }
                ).then(res => {
                    return axios.get('https://api.hubapi.com/settings/v3/users/', {
                        headers: {'Authorization': `Bearer ${res.data}`}
                    })
                });
                setUsers(result.data.results);
            }
        });
    }

    async function DeleteUser(id) {
        console.log(`DELETE USER ${id}`);
        const result = await axios.post('http://localhost:3000/deleteUser', {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
                'Content-Type': 'application/json'
            },
            params: {
                'id': id
            },
        }).then(async res => {
            console.log("User Deleted!!");
            console.log(res);
            if (res.data) {
                const result = await axios.get('http://localhost:3000/getAccess', {
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
                        }
                    }
                ).then(res => {
                    return axios.get('https://api.hubapi.com/settings/v3/users/', {
                        headers: {'Authorization': `Bearer ${res.data}`}
                    })
                });
                setUsers(result.data.results);
            }
        });
    }

    useEffect(async () => {
        const result = await axios.get('http://localhost:3000/getAccess', {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
                }
            }
        ).then(res => {
            return axios.get('https://api.hubapi.com/settings/v3/users/', {
                headers: {'Authorization': `Bearer ${res.data}`}
            })
        });
        setUsers(result.data.results);
    }, []);

    return (
        <>
            <Header/>
            <Container className="mt--7" fluid>
                <Row>
                    <div className="col">
                        <Card className="shadow">
                            <CardHeader className="border-0">
                                <h3 className="mb-0">Nutzer</h3>
                            </CardHeader>
                            <Table className="align-items-center table-flush" responsive>
                                <thead className="thead-light">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Email</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                {users.map(user => {
                                    return <tr key={user.id}>
                                        <td>{user.id}</td>
                                        <td>{user.email}</td>
                                        <td><Button onClick={() => DeleteUser(user.id)}>Benutzer Löschen</Button>
                                        </td>
                                    </tr>
                                })}
                                </tbody>
                            </Table>
                            <CardFooter className="py-4">
                                <div className="AddUser">
                                    <Input onChange={(e) => setUserWelcomeEmail(e.target.checked)} className={"check"}
                                           type={"checkbox"} bsSize={'sm'}/>
                                    <Input onChange={(e) => setUserEmail(e.target.value)} placeholder={"Email"}
                                           bsSize={'sm'}/>
                                    <Button onClick={AddUser}>Benutzer Hinzufügen</Button>
                                </div>
                            </CardFooter>
                        </Card>
                    </div>
                </Row>
            </Container>
        </>
    );
}

export default Tables;
